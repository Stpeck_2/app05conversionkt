package com.example.app05conversionkt

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.app05conversionkt.R
import android.content.Intent
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity() {
    var IMC: Button? = null
    var convertidor: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        IMC = findViewById<View>(R.id.btnimc) as Button
        IMC!!.setOnClickListener {
            val i = Intent(this@MainActivity, imc::class.java)
            startActivity(i)
        }
        convertidor = findViewById<View>(R.id.btnconvertidor) as Button
        convertidor!!.setOnClickListener {
            val i = Intent(this@MainActivity, grado::class.java)
            startActivity(i)
        }
    }
}