package com.example.app05conversionkt

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class imc : AppCompatActivity() {
    var editnombre: EditText? = null
    var editaltura: EditText? = null
    var editpeso: EditText? = null
    private var btnlimpiar: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_imc)
        editnombre = findViewById(R.id.editnombre)
        editaltura = findViewById(R.id.editaltura)
        editpeso = findViewById(R.id.editpeso)
        btnlimpiar = findViewById(R.id.btnlimpiar) as Button?
    }

    fun imc(vista: View?) {
        val nombre = editnombre!!.text.toString()
        val altura = editaltura!!.text.toString()
        val peso = editpeso!!.text.toString()
        if (nombre == "" || altura == "" || peso == "") {
            Toast.makeText(getApplicationContext(), "Por favor llene los datos", Toast.LENGTH_LONG)
                .show()
        } else {
            //int nmb = Integer.parseInt(nombre);
            var alt = altura.toFloat()
            val kg = peso.toFloat()
            alt = alt / 100
            //Proceso de calcular el indice de masa corporal
            val imc = kg / Math.pow(alt.toDouble(), 2.0).toFloat()
            //Convirtiendo el resultado a texto
            val imcTexto = String.format("%.1f", imc)
            if (imc < 18.5f) Toast.makeText(
                getApplicationContext(), "nombre: " + nombre +
                        " y su indice de masa corporal es de: " + imcTexto + " usted tiene Desnutricion",
                Toast.LENGTH_LONG
            ).show() // Desnutrición
            if (imc >= 18.5f && imc < 25) Toast.makeText(
                getApplicationContext(),
                "nombre: " + nombre +
                        " y su indice de masa corporal es de: " + imcTexto + " usted esta Normal",
                Toast.LENGTH_LONG
            ).show() // Normal
            if (imc >= 25 && imc < 30) Toast.makeText(
                getApplicationContext(), "nombre: " + nombre +
                        " y su indice de masa corporal es de: " + imcTexto + " usted tiene Sobrepeso",
                Toast.LENGTH_LONG
            ).show() //Sobrepeso
            if (imc >= 30 && imc < 35) Toast.makeText(
                getApplicationContext(), "nombre: " + nombre +
                        " y su indice de masa corporal es de: " + imcTexto + " usted tiene Obesidad Grado 1",
                Toast.LENGTH_LONG
            ).show() // Obestdad Grado1
            if (imc >= 35 && imc < 40) Toast.makeText(
                getApplicationContext(), "nombre: " + nombre +
                        " y su indice de masa corporal es de: " + imcTexto + " usted tiene Obesidad Grado 2",
                Toast.LENGTH_LONG
            ).show() // Obesidod Grado2
            if (imc >= 40) Toast.makeText(
                getApplicationContext(), "nombre: " + nombre +
                        " y su indice de masa corporal es de: " + imcTexto + " usted tiene Obesidad Grado 3",
                Toast.LENGTH_LONG
            ).show() // Obesidod Grado3
        }
        btnlimpiar!!.setOnClickListener {
            editnombre!!.setText("")
            editaltura!!.setText("")
            editpeso!!.setText("")
        }
    }
}