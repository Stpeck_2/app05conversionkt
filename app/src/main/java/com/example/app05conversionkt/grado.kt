package com.example.app05conversionkt

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class grado : AppCompatActivity() {
    var convertir: Button? = null
    var cantidad: EditText? = null
    var resultado: TextView? = null
    var spincl: Spinner? = null
    private var btnlimpiarG: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grado)
        convertir = findViewById(R.id.btntem) as Button?
        cantidad = findViewById(R.id.cant) as EditText?
        resultado = findViewById(R.id.Resu) as TextView?
        spincl = findViewById(R.id.spinla) as Spinner?
        btnlimpiarG = findViewById(R.id.btnlimpiarG) as Button?
        val op = arrayOf(" seleccione una opcion ", " °C a °F", "°F a °C")
        val adapter: ArrayAdapter<String> =
            ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, op)
        spincl!!.adapter = adapter
        convertir!!.setOnClickListener {
            if (cantidad!!.text.toString() == "") {
                val msg = Toast.makeText(
                    getApplicationContext(),
                    "Escribe una cantidad ",
                    Toast.LENGTH_SHORT
                )
                msg.show()
            } else {
                val c = cantidad!!.text.toString().toDouble()
                var res: Double? = null
                val select = spincl!!.selectedItemPosition
                when (select) {
                    0 -> {
                        res = 0.0
                        Toast.makeText(
                            getApplicationContext(),
                            "Seleccione una opcion",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    1 -> res = 1.8 * c + 32
                    2 -> res = (c - 32) / 1.8
                }
                resultado!!.text = res.toString()
            }
        }
        btnlimpiarG!!.setOnClickListener {
            cantidad!!.setText("")
            resultado!!.text = ""
        }
    }
}